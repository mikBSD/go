// Copyright 2019 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build freebsd
// +build ppc64le ppc64

package runtime

//go:nosplit
func (th *vdsoTimehands) getTimecounter() (uint32, bool) {
	return 0, false
}
